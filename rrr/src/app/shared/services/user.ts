export interface User {
    uid: string;
    email: string;
    displayName: string;
    photoURL: string;
    emailVerified: boolean;
}

export interface BestUser {
    boxesDestroyed: number;
    bridgesDestroyed: number;
    coinsCollected: number;
    level: number;
    monstersKilled: number;
    monstersKills: number;
    playedTime: number;
    powerUps: number;
    score: number;
    spikes_water: number;
}

export interface GeneralUser {
    boxesDestroyed: number;
    bridgesDestroyed: number;
    coinsCollected: number;
    monstersKilled: number;
    monstersKills: number;
    playedTime: number;
    powerUps: number;
    spikes_water: number;
}
