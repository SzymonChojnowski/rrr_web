import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from "@angular/router";
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { BestUser } from "../../shared/services/user";
import { GeneralUser } from "../../shared/services/user";
import { AuthService }  from "../../shared/services/auth.service";
@Injectable({
  providedIn: 'root'
})
export class UserService {

  bestList: AngularFireList<any>;
  generarList: AngularFireList<any>;
  leaderboard: AngularFireList<any>;

  constructor(private Firebase: AngularFireDatabase,
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,  
    public ngZone: NgZone, // NgZone service to remove outside scope warning
    private firebase: AngularFireDatabase,
    public authService: AuthService
  ) {} 

  getBestStats(uid)
  {
    this.bestList = this.firebase.list(uid+"/best");
    return this.bestList.snapshotChanges();
  }
  getGeneralStats(uid)
  {
    this.generarList = this.firebase.list(uid+"/general");
    return this.generarList.snapshotChanges();
  }
  getLeaderBoard() {
    this.leaderboard = this.firebase.list("leaders", ref => {
      return ref.orderByChild('score');
    });
    return this.leaderboard.snapshotChanges();
  }
  getNick(uid)
  {
    return this.firebase.object(uid).snapshotChanges();
  }
}
