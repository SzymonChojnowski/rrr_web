import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from "../../shared/services/auth.service";
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../../shared/services/user.service'
import { BestUser, GeneralUser } from 'src/app/shared/services/user';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  constructor(
    public authService: AuthService,
    public router: Router,
    public ngZone: NgZone,
    public userService: UserService,
    private route: ActivatedRoute

  ) { }
  bestArray = [];
  generalArray: GeneralUser[] = [];
  bestNames = [["Destroyed", "Boxes"],["Destroyed", "Bridges"],["Collected", "Coins"],["Maximum Level", ""],["Killed", "Monsters"],["Monsters have tried to kill you", "times"],["Played Time", ""],["Power-ups collected", "times"],["Score", ""],["Entered spikes and water", "times"]];
  generalNames = [["Destroyed", "Boxes"],["Destroyed", "Bridges"],["Collected", "Coins"],["Killed", "Monsters"],["Monsters have tried to kill you", "times"],["Played Time", ""],["Power-ups collected", "times"],["Entered spikes and water", "times"]];

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      let id = params['id']; // (+) converts string 'id' to a number
      this.getStats(id);
    });
   
    
  }

  getStats(id: string){
  
    this.userService.getBestStats(id).subscribe(
      list => {
        this.bestArray = list.map(item => {
          console.log(            item.key, item.payload.val()           )
          return  item.payload.val();
        });
        console.log(           this.bestArray         )
      }
    );
    this.userService.getGeneralStats(id).subscribe(
      list => {
        this.generalArray = list.map(item => {
          console.log(item.payload.val())
          return item.payload.val()
        });
      }
    );
  };

}


