import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from "../../shared/services/auth.service";
import { Router } from "@angular/router";
import { Observable } from 'rxjs';
import { UserService } from '../../shared/services/user.service'
import { BestUser, GeneralUser } from 'src/app/shared/services/user';

@Component({
  selector: 'app-my-stats',
  templateUrl: './my-stats.component.html',
  styleUrls: ['./my-stats.component.css']
})
export class MyStatsComponent implements OnInit {

  constructor(
    public authService: AuthService,
    public router: Router,
    public ngZone: NgZone,
    public userService: UserService
  ) { }
  bestArray = [];
  generalArray: GeneralUser[] = [];
  bestNames = [["Destroyed", "Boxes"],["Destroyed", "Bridges"],["Collected", "Coins"],["Maximum Level", ""],["Killed", "Monsters"],["Monsters have tried to kill you", "times"],["Played Time", ""],["Power-ups collected", "times"],["Score", ""],["Entered spikes and water", "times"]];
  generalNames = [["Destroyed", "Boxes"],["Destroyed", "Bridges"],["Collected", "Coins"],["Killed", "Monsters"],["Monsters have tried to kill you", "times"],["Played Time", ""],["Power-ups collected", "times"],["Entered spikes and water", "times"]];

  ngOnInit() {
    this.userService.getBestStats(this.authService.uid).subscribe(
      list => {
        this.bestArray = list.map(item => {
          console.log(            item.key, item.payload.val()           )
          return  item.payload.val();
        });
        console.log(           this.bestArray         )
      }
    );
    this.userService.getGeneralStats(this.authService.uid).subscribe(
      list => {
        this.generalArray = list.map(item => {
          console.log(item.payload.val())
          return item.payload.val()
        });
      }
    );
    console.log(this.bestArray.length);
    
  }
}
