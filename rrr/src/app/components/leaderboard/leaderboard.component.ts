import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from "../../shared/services/auth.service";
import { Router } from "@angular/router";
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {

  leaderboard = [];

  constructor(
    public authService: AuthService,
    public router: Router,
    public ngZone: NgZone,
    public userService: UserService
  ) { }

  ngOnInit() {
    this.userService.getLeaderBoard().subscribe(
      list => {
        this.leaderboard = list.map(item => {
          let body = {
            id: item.key,
            score: item.payload.val(),
            nick: null
          }

          this.userService.getNick(item.key).subscribe( value => {
            let data = value.payload.val();
            body.nick = data['nickname'];
            });
            return body;
          }

          ).reverse();
      }
    );
  }
}
