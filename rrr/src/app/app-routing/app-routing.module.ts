import { NgModule } from '@angular/core';
// Required services for navigation
import { Routes, RouterModule } from '@angular/router';

// Import all the components for which navigation service has to be activated 
import { SignInComponent } from './../components/sign-in/sign-in.component';
import { SignUpComponent } from './../components/sign-up/sign-up.component';
import { DashboardComponent } from './../components/dashboard/dashboard.component';
import { ForgotPasswordComponent } from './../components/forgot-password/forgot-password.component';
// Import canActivate guard services
import { AuthGuard } from "./../shared/guard/auth.guard";
import { SecureInnerPagesGuard } from "./../shared/guard/secure-inner-pages.guard";
import { LeaderboardComponent } from '../components/leaderboard/leaderboard.component';
import { MyStatsComponent } from '../components/my-stats/my-stats.component';
import { UserDetailsComponent } from '../components/user-details/user-details.component';

const routes: Routes = [
  { path: '', redirectTo: '/sign-in', pathMatch: 'full'},
  { path: 'sign-in', component: SignInComponent, canActivate: [SecureInnerPagesGuard]},
  { path: 'register-user', component: SignUpComponent, canActivate: [SecureInnerPagesGuard]},
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'leaderboard', component: LeaderboardComponent, canActivate: [AuthGuard] },
  { path: 'my-stats', component: MyStatsComponent, canActivate: [AuthGuard] },
  { path: 'user-details/:id', component: UserDetailsComponent, canActivate: [AuthGuard] },
  { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [SecureInnerPagesGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }