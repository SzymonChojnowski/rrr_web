// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    apiKey: "AIzaSyAFCPo6cFUUS6W5Ie6YZqeoC3WRPr17W-4",
    authDomain: "engineeringthesis-pp.firebaseapp.com",
    databaseURL: "https://engineeringthesis-pp.firebaseio.com",
    projectId: "engineeringthesis-pp",
    storageBucket: "engineeringthesis-pp.appspot.com",
    messagingSenderId: "189598356510"
  },
  production:
  {}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
